/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        powergrotesk: ["PowerGrotesk", "sans-serif"],
        poppins: ["Poppins", "sans-serif"],
        luckiest: ["Luckiest Guy", "cursive"],
      },
    },
    colors: {
      white: "#ffffff",
      red: "#FF2222",
    },
  },
  plugins: [],
};
