For this collab let's:

- make a returns calculator (see description in /pages/presale.tsx) on the front end

- create a back end script that stores presale rounds to the database: see /scripts/create_presalerounds.ts
