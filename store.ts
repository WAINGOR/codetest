import {
  TypedUseSelectorHook,
  useSelector as useReduxSelector,
} from "react-redux";

import { configureStore, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { State } from "./types";

const initialState: State = {
  user: {},
};

export { useDispatch } from "react-redux";

// Export a typed version of the useSelector hook
export const useSelector: TypedUseSelectorHook<State> = useReduxSelector;
