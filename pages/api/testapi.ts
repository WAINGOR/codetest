import type { NextApiRequest, NextApiResponse } from "next";
import { saveAndGetDocument, getDocuments } from "../../helpers";

type Data = {
  name: string;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  const test = await saveAndGetDocument("test", { a: 22 });

  // console.log("test");
  // console.log(test);
  const records = await getDocuments("test");
  console.log(records)
  res.status(200).json({ name: "John Doe" });
}
