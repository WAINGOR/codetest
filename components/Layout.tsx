import * as React from "react";
import Menu from "./Menu";
import { connectMetaMaskWallet } from "../redux/services";
import { useDispatch } from "react-redux";
import { useEffect, useCallback } from "react";
import 'react-toastify/dist/ReactToastify.css';

export interface ContainerProps {
  children: React.ReactNode;
  backgroundImage?: string;
}

export default function Layout({ children }: ContainerProps) {
  const dispatch = useDispatch();

  const connectWallet = useCallback(() => {
    connectMetaMaskWallet(dispatch);
  }, [dispatch]);

  useEffect(() => {
    connectWallet();
  }, [connectWallet])

  return (
    <>
      <Menu />
      <main>
        <header className="[&_p]:text-[20px] [&_button]:text-[20px] h-[100vh]">
          <div
            className="absolute bg-cover no-repeat h-[100vh] w-[100vw] pt-[80px] bg-top"
            style={{
              backgroundImage: `url('/img/island-68081_1920.png')`,
            }}
          />
          {children}
          <img src="/img/bodershape.png"
            className="absolute bottom-0 object-cover w-full" alt={""} />
        </header>
      </main>
      
    </>
  );
}
