import { useEffect, useRef, useCallback, FC } from "react";
import Link from "next/link";
import { useSelector, useDispatch } from "react-redux";
import { connectMetaMaskWallet } from "../redux/services";
import { State } from "../types";

const Menu = () => {
  const menuRef = useRef<null | HTMLDivElement>(null);
  const backgroundRef = useRef<null | HTMLDivElement>(null);
  const dispatch = useDispatch();

  const account = useSelector((state: State) => state.account);
  const error = useSelector((state: State) => state.error)

  const connectWallet = useCallback(() => {
    connectMetaMaskWallet(dispatch);
  }, [dispatch]);

  useEffect(() => {
    window.addEventListener("scroll", function () {
      if (
        document.body.scrollTop > 20 ||
        document.documentElement.scrollTop > 20
      ) {
        menuRef.current!!.style.height = "60px";
        backgroundRef.current!!.style.opacity = "0.9";
        return;
      }
      menuRef.current!!.style.height = "80px";
      backgroundRef.current!!.style.opacity = "1";
    });
  }, []);

  return (
    <nav className="fixed top-0 left-0 z-50 w-full">
      <div className="absolute w-full border-b-[1px] border-white bottom-0 opacity-30"></div>
      <div
        ref={backgroundRef}
        className="absolute z-0 w-full h-full  transition-[opacity]"
      />
      <div
        ref={menuRef}
        className=" transition-[height] duration-500 ease-in-out relative mx-auto flex h-[80px] max-w-[1380px] items-center justify-between px-8 sm:px-12 lg:border-transparent"
      >
        <div className=" flex justify-center  ">
          <div className="hidden cursor-pointer lg:block"></div>
        </div>
        <div className="hidden items-center justify-center whitespace-nowrap  text-[14px] font-medium text-white lg:ml-4  lg:flex lg:space-x-1 lg:text-[14px] xl:space-x-4">
          <div className="hidden items-center text-[14px] font-medium text-black  lg:ml-8 lg:flex lg:space-x-8 xl:ml-20 xl:space-x-14 xl:text-[14px] ">
            <Link className="py-2" href="/">
              Home
            </Link>
            <Link className="py-2" href="/presale">
              Presale
            </Link>
            <div>
              <button
                onClick={connectWallet}
                className="text-white text-[13px] relative flex items-center whitespace-nowrap mx-3 justify-center  min-w-[140px] rounded-[100px] border-2   py-2 transition-all duration-500 after:hidden hover:bg-transparent">
                {account ? (account.slice(0, 10) + '...') : "Connect Wallet"}
              </button>
              {error !== null && <p className="text-red">{error}</p>}
            </div>
          </div>
        </div>
        <button
          className="inline-flex items-center justify-center text-black lg:hidden"
          id="headlessui-disclosure-button-:r0:"
          type="button"
          aria-expanded="false"
        >
          <span className="sr-only">Open main menu</span>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 20 20"
            fill="currentColor"
            aria-hidden="true"
            className="block h-8 w-8"
          >
            <path
              fillRule="evenodd"
              d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
              clipRule="evenodd"
            ></path>
          </svg>
        </button>
      </div>
    </nav>
  );
};

export default Menu