import * as React from "react";

export interface ContainerProps {
  children: React.ReactNode;
  backgroundImage?: string;
}

const Container = ({ children, backgroundImage }: ContainerProps) => (
  <div
    className="min-h-[90vh] relative flex flex-col items-center justify-center bg-cover bg-bottom px-6 py-20 lg:px-12 lg:py-32 "
    style={
      backgroundImage
        ? { backgroundImage: `url('/img/${backgroundImage}')` }
        : {}
    }
  >
    {children}
  </div>
);

export default Container;
